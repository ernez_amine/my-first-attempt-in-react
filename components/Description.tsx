
type DetailsParam = {
    text: string | null,
    ok: string,
}

function Details({ok ,text}: DetailsParam) {
    return (
        <div>
            <h1>
                {ok}
            </h1>
            <h2>
                {text}
            </h2>
        </div>
    )
}

function Description() {
    return (
        <div>
            <h3>
                Description
                <Details ok="hello from details to description" text="everything is ok on this side "/>
            </h3>
        </div>
    )
}

export default Description