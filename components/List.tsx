type ListParams ={
    array:Array<string>
}

function List ({array} :ListParams){
    return (
        <div>
            {array.map((item, index) => (
                <p key={index}>{item}</p>
            ))}
        </div>
    )
}

export default List