import styles from './page.module.css'


type HeaderParams = {
    description: String | null
}

function Header(props:HeaderParams) {
    return (
        <main>
            <p>header </p>
            <p>{props.description}</p>
        </main>
    )
}

export default Header
