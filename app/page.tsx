import styles from './page.module.css'
import Header from "@/components/Header";
import Description from "@/components/Description";
import List from "@/components/List";
import Link from "next/link";




function Home() {


    return (

        <main className={styles.section} >
            <div>
                <Header description="all the description goes right here" />
                 <Description/>
                <p>hello </p>
                <List array={["this site is on work","its ok to be a react developer"]} />
                <Link href='/about'>Go to about page </Link>
            </div>

        </main>
    )
}

export default Home;
